import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-prova',
  templateUrl: './prova.component.html',
  styleUrls: ['./prova.component.css']
})
export class ProvaComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  dato:string = "";
  datiMeteo:any[]=[];
  
  onClickProva(){
    this.datiMeteo.push("");
  }
  onSubmit(frmDati: any) {
    console.log(frmDati);
  }
}