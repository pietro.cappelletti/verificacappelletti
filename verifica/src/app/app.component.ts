import { Component } from '@angular/core';
import { DatiService } from './servizi/dati.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'verifica';
  dato:number = 0;
  datiMeteo:any[]=[];
  // dichiarato e creato un oggetto di tipo DatiService
  // DI --> dependency Injectoin
  constructor(private datiService:DatiService){}

  onClick(){
    this.datiService.getDatiMeteo().subscribe(
      (dati:any[]) => this.datiMeteo=dati
    )
    this.dato = this.datiService.getDato();
  }
}