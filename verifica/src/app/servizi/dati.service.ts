import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class DatiService {
  private dato:number = 0;
  constructor(private http:HttpClient) { }

  getDato(){
    return this.dato;
  }
  getDatiMeteo(){
    return this.http.get('https://jsonplaceholder.typicode.com/comments')
  }
}
